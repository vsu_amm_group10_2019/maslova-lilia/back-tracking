﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Domino
{
    public partial class Form1 : Form
    {
        Dominos[] domino;
        public Form1()
        {
            InitializeComponent();
        }

        private void загрузитьФайлToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openfile = new OpenFileDialog();
            StreamReader openedFile;
            openfile.Filter = "All Files (*.*)|*.*";
            if (openfile.ShowDialog() == DialogResult.OK)
            {
                openedFile = new StreamReader(openfile.FileName, Encoding.UTF8);
                richTextBox1.Text = openedFile.ReadToEnd();
                openedFile.Close();
            }
        }

        private void выполнитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            domino = new Dominos[richTextBox1.Lines.Length];
            for (int i = 0; i < domino.Length; i++)
            {
                domino[i] = new Dominos();
            }
            for (int i = 0; i < richTextBox1.Lines.Length; i++)
            {
                char[] s = richTextBox1.Lines[i].ToCharArray();
                domino[i].Fside = s[0] - '0';
                domino[i].Sside = s[2] - '0';
            }
            fish(domino, 0);

            int maxPrior = 0;
            for (int i = 0; i < domino.Length; i++)
            {
                if ((domino[i].priority > maxPrior))
                {
                    maxPrior = domino[i].priority;
                }
            }
            if (maxPrior != domino.Length)
            {
                for (int i = 0; i < domino.Length; i++)
                {
                    for (int j = 0; j < domino.Length; j++)
                    {
                        if ((domino[j].priority == i + 1) && (domino[j].used == true))
                        {
                            richTextBox2.AppendText(domino[j].Fside + "/" + domino[j].Sside + ", ");
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Рыбы нет", "Уведомление", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void fish(Dominos[] domino, int prior)
        {
            for (int i = 0; i < domino.Length; i++)
            {
                if (domino[i].used == false)
                {
                    if (prior == 0)
                    {
                        prior++;
                        domino[i].priority = prior;
                        domino[i].used = true;
                        fish(domino, prior);
                        int maxPrior = prior;
                        for (int j = 0; j < domino.Length; j++)
                        {
                            if ((domino[j].priority > maxPrior))
                            {
                                maxPrior = domino[j].priority;
                            }
                        }
                        if (maxPrior <= prior)
                        {
                            domino[i] = reverse(domino[i]);
                            fish(domino, prior);
                        }
                    }
                    else
                    {
                        for (int j = 0; j < domino.Length; j++)
                        {
                            if ((domino[j].priority == prior) && (domino[i].used == false))
                            {
                                if (domino[i].Fside == domino[j].Sside)
                                {
                                    prior++;
                                    domino[i].priority = prior;
                                    domino[i].used = true;
                                    fish(domino, prior);
                                }
                                else
                                {
                                    domino[i] = reverse(domino[i]);
                                    if (domino[i].Fside == domino[j].Sside)
                                    {
                                        domino[i] = reverse(domino[i]);
                                        prior++;
                                        domino[i].priority = prior;
                                        domino[i].used = true;
                                        fish(domino, prior);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        private Dominos reverse(Dominos a)
        {
            int c;
            c = a.Fside;
            a.Fside = a.Sside;
            a.Sside = c;
            Dominos b;
            b = a;
            return b;
        }

        private void сохранитьФайлToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "All Files (*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(richTextBox1.Text, dialog.FileName);
            }
        }
    }
}
